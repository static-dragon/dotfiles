# dotfiles

## How to use
Everything except the i3 folder goes in your home directory, while i3 goes into your ~/.config folder
or you may use the automated installer if you wish

## Requirements for .Xresources
For fonts pertaining to rofi and Urxvt, you need the [Ofice Code Pro](https://github.com/nathco/Office-Code-Pro) fonts
Obviously you will need rofi and Urxvt for the .Xresources changes to apply to them

## Requirements for vim changes
Vim Requires powerline fonts, and lua for the plugins to function correctly
Also the following plugins:

[Pathogen by tpope](https://github.com/tpope/vim-pathogen) (Required for all below plugins)

[vim-fugitive by tpope](https://github.com/tpope/vim-fugitive)

[Supertab by ervandew](https://github.com/ervandew/supertab)

[Vim-Airline](https://github.com/vim-airline/vim-airline)

[Neocomplete by Shougo](https://github.com/Shougo/neocomplete.vim)

[NERDTree by scrooloose](https://github.com/scrooloose/nerdtree)

[NERDTRee-tabs by jistr](https://github.com/jistr/vim-nerdtree-tabs)

[Vim-litecorrect by reedes](https://github.com/reedes/vim-litecorrect)

[vim-pencil by reedes](https://github.com/reedes/vim-pencil)

[vim-snipmate by garbas](https://github.com/garbas/vim-snipmate)

[gruvbox by morhetz](https://github.com/morhetz/gruvbox)


## NOTE
Anyone wishing to fork any of my dotfiles, feel free to create a seperate branch, if the changes are something that I would find use of and actually use in my workflow, I will integrate it into the master branch with your permission

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
